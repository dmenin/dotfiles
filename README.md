# Dario's Dotfiles

## Symlinks setup

Execute:

```sh
stow "<folder>"
```

for example:

```sh
stow vim/
```

or

```sh
stow */
```

## Adding a new configuration file

Example with `git`:

```sh
mkdir git
touch git/.gitconfig_extended
touch git/.gitignore_global
stow --adopt -nv git # Check what stow wants to do without applying changes, with the "-n" flag
stow --adopt -v git  # Apply changes, copy the original files with the same names of the ones we just created to the "git" directories, and replace them with symlinks
```

*Note:* it's useful to split config files to avoid copying sensitive informations, for example, for git we can put

```conf
[include]
  path = ~/.gitconfig_extended
```

inside `~/.gitconfig`.
